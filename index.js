const express = require('express');
const cors = require('cors');
const dotenv = require('dotenv');

const app = express();
const mongoose = require('mongoose');
const graphqlHTTP = require('express-graphql');

dotenv.config();

app.use(express.json());


app.get('/', (req, res) => res.send('Hello there, node app here!'));

app.listen(4001, () => console.log("Listening on port 4001"));
